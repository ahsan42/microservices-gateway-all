package com.ontech.onapp.service;

import com.ontech.onapp.entity.User;

public interface UserService {

    User registerUser(User input);

    Iterable<User> findAll();
}
