package com.ontech.onapp.repository;

import com.ontech.onapp.entity.User;
import org.springframework.data.repository.CrudRepository;


public interface UserRepository extends CrudRepository<User, Long> {

}