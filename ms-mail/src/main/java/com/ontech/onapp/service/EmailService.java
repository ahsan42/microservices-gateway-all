package com.ontech.onapp.service;

import com.ontech.onapp.entity.dto.UserDto;

public interface EmailService {

    void sendSimpleMessage(UserDto input);
}
