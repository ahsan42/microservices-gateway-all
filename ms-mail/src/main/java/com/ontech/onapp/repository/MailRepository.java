package com.ontech.onapp.repository;

import com.ontech.onapp.entity.Mail;
import org.springframework.data.repository.CrudRepository;


public interface MailRepository extends CrudRepository<Mail, Long> {

}